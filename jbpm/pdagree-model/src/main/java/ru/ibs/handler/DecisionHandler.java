package ru.ibs.handler;

import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import ru.ibs.model.PersonalDataRequest;

@Slf4j
public class DecisionHandler implements WorkItemHandler {

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {

        PersonalDataRequest request = (PersonalDataRequest) workItem.getParameter("request");

        log.info("Обработка результата пользователя {}", request.getPerson());
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {

    }
}
