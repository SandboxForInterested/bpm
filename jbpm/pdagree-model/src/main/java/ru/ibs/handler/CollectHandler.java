package ru.ibs.handler;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import ru.ibs.model.PersonalDataRequest;

public class CollectHandler implements WorkItemHandler {

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        String personName = (String) workItem.getParameter("name");
        String peronMail = (String) workItem.getParameter("mail");
        String companyName = (String) workItem.getParameter("company");
        String projectName = (String) workItem.getParameter("project");

        PersonalDataRequest request = new PersonalDataRequest(personName, peronMail, companyName, projectName);

        Map<String, Object> contextOfObject = new HashMap<>();
        contextOfObject.put("request", request);

        workItemManager.completeWorkItem(workItem.getId(), contextOfObject);
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {

    }
}
