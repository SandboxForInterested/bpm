package ru.ibs.model;

import java.io.Serializable;
import java.util.UUID;

public class PersonalDataRequest implements Serializable {

    private final String id;

    private String person;

    private String mail;

    private String company;

    private String project;

    public PersonalDataRequest(String person, String mail, String company, String project) {
        this.id = UUID.randomUUID().toString();
        this.person = person;
        this.mail = mail;
        this.company = company;
        this.project = project;
    }

    public String getId() {
        return id;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
