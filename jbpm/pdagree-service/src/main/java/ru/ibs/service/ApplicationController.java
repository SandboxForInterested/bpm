package ru.ibs.service;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.kie.server.services.api.KieServer;
import org.kie.server.springboot.jbpm.ContainerAliasResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ApplicationController {

    @Autowired
    private DeploymentService deploymentService;

    @Autowired
    private RuntimeDataService runtimeDataService;

    @Autowired
    private ProcessService processService;

    @Autowired
    private KieServer kieServer; // have to autowire this bean in order to use aliasresolver atm

    @Autowired
    private ContainerAliasResolver aliasResolver;
    private String containerAlias = "pdagree-kjar";
    private String processId = "pd_agreement.pd_agreement";

    @PostMapping("/start")
    public String startProcess(@RequestBody Map<String,Object> params) {

        String resolvedContainerId = aliasResolver.latest(containerAlias);

        long processInstanceId = processService.startProcess(resolvedContainerId,
            processId,
            params);

        log.info("Стартовал процесс с идентификатором {}", processInstanceId);
        return String.valueOf(processInstanceId);
    }

    @GetMapping(value = "/event/{message}/{processId}")
    public void confirmEvent(@PathVariable String message,
        @PathVariable String processId,
        @RequestParam Map<String, Object> params) {

        processService.signalProcessInstance(Long.valueOf(processId), message, params);

    }

}
