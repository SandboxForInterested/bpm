package ru.ibs.activity;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.Map;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
public class ApplicationController {

    private final String processDefinitionKey = "PDAgree";

    private final RuntimeService runtimeService;

    @PostMapping("/start")
    public String startProcess(@RequestBody Map<String, Object> params) {

        ProcessInstance processInstance
            = runtimeService.createProcessInstanceBuilder()
            .processDefinitionKey(processDefinitionKey)
            .variables(params)
            .start();

        log.info("Стартовал процесс с идентификатором {}", processInstance.getProcessDefinitionId());
        return String.valueOf(processInstance.getProcessDefinitionId());
    }

    @GetMapping(value = "/event/{message}/{processId}")
    public void confirmEvent(@PathVariable String message,
        @PathVariable String processId,
        @RequestParam Map<String, Object> params) {

        String executionId = runtimeService.createExecutionQuery()
            .parentId(processId).messageEventSubscriptionName(message).singleResult().getId();

        runtimeService.messageEventReceived(message, executionId, params);
    }
}
