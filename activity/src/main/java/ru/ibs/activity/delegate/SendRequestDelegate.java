package ru.ibs.activity.delegate;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SendRequestDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {

        String processId = execution.getProcessInstanceId();

        log.info("Необходимо перейти по ссылка чтобы подтвердить использование персональных данных");
        log.info("http://localhost:8080/event/message/{}?accept=true", processId);

    }
}
