package ru.ibs.activity.delegate;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DecisionDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {

        String accept = (String)execution.getVariable("accept");

        if(accept == null){
            log.info("Ответ от пользователя не получен");
        }
        else if (Boolean.TRUE.equals(Boolean.valueOf(accept))) {
            log.info("Пользователь согласился предоставить персональные данные");
        } else {
            log.info("Пользователь отказался предоставить персональные данные");
        }

    }
}
