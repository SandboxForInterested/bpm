package ru.ibs.camunda;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
public class ApplicationController {

    private final String processDefinitionKey = "PDAgree";

    private final RuntimeService runtimeService;

    @PostMapping("/start")
    public String startProcess(@RequestBody Map<String, Object> params) {

        ProcessInstance processInstance
            = runtimeService.createProcessInstanceByKey(processDefinitionKey)
            .setVariables(params).execute();

        log.info("Стартовал процесс с идентификатором {}", processInstance.getProcessDefinitionId());
        return String.valueOf(processInstance.getProcessDefinitionId());
    }

    @GetMapping(value = "/event/{message}/{processId}")
    public void confirmEvent(@PathVariable String message,
        @PathVariable String processId,
        @RequestParam Map<String, Object> params) {
        runtimeService
            .createMessageCorrelation(message)
            .processInstanceId(processId)
            .setVariables(params)
            .correlate();
    }
}
