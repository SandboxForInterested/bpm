package ru.ibs.flowable.delegate;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CollectorDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) {

        Map<String,Object> variables = execution.getVariables();

        log.info("Получены параметры: {}", variables);
    }
}
