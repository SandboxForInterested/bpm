# BPM

## Цель проекта

Реализация процесса на разных BPMN движках.
Подробнее можно прочитать [тут](https://habr.com/)

### Диаграмма

<img src="./diag_pic.png" alt="Процесс согласования">

## Используемые BPM

- [ ] [jBPM](https://www.jbpm.org/)
- [ ] [Activity](https://www.activiti.org/)
- [ ] [Flowable](https://www.flowable.com/)
- [ ] [Camunda](https://camunda.com/)
